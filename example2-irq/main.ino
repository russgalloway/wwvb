// https://github.com/vinmarshall/WWVB-Clock

#include <stdio.h>

boolean debug = true;
int lightPin = 13;
int wwvbInPin = 2;

char *months[12] = {
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"};

int eomYear[14][2] = {
    {0, 0},     // Begin
    {31, 31},   // Jan
    {59, 60},   // Feb
    {90, 91},   // Mar
    {120, 121}, // Apr
    {151, 152}, // May
    {181, 182}, // Jun
    {212, 213}, // Jul
    {243, 244}, // Aug
    {273, 274}, // Sep
    {304, 305}, // Oct
    {334, 335}, // Nov
    {365, 366}, // Dec
    {366, 367}  // overflow
};

unsigned long pulseStartTime = 0;
unsigned long pulseEndTime = 0;
unsigned long frameEndTime = 0;
unsigned long lastRtcUpdateTime = 0;
boolean bitReceived = false;
boolean wasMark = false;
int framePosition = 0;
int bitPosition = 1;
char lastTimeUpdate[17];
char lastBit = ' ';
int errors[10] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
int errIdx = 0;
int bitError = 0;
boolean frameError = false;

// RTC clock variables
byte second = 0x00;
byte minute = 0x00;
byte hour = 0x00;
byte day = 0x00;
byte date = 0x01;
byte month = 0x01;
byte year = 0x00;
byte ctrl = 0x00;

// WWVB time variables
byte wwvb_hour = 0;
byte wwvb_minute = 0;
byte wwvb_day = 0;
byte wwvb_year = 0;

struct wwvbBuffer
{
  unsigned long long U12 : 4;      // no value, empty four bits only 60 of 64 bits used
  unsigned long long Frame : 2;    // framing
  unsigned long long Dst : 2;      // dst flags
  unsigned long long Leapsec : 1;  // leapsecond
  unsigned long long Leapyear : 1; // leapyear
  unsigned long long U11 : 1;      // no value
  unsigned long long YearOne : 4;  // year (5 -> 2005)
  unsigned long long U10 : 1;      // no value
  unsigned long long YearTen : 4;  // year (5 -> 2005)
  unsigned long long U09 : 1;      // no value
  unsigned long long OffVal : 4;   // offset value
  unsigned long long U08 : 1;      // no value
  unsigned long long OffSign : 3;  // offset sign
  unsigned long long U07 : 2;      // no value
  unsigned long long DayOne : 4;   // day ones
  unsigned long long U06 : 1;      // no value
  unsigned long long DayTen : 4;   // day tens
  unsigned long long U05 : 1;      // no value
  unsigned long long DayHun : 2;   // day hundreds
  unsigned long long U04 : 3;      // no value
  unsigned long long HourOne : 4;  // hours ones
  unsigned long long U03 : 1;      // no value
  unsigned long long HourTen : 2;  // hours tens
  unsigned long long U02 : 3;      // no value
  unsigned long long MinOne : 4;   // minutes ones
  unsigned long long U01 : 1;      // no value
  unsigned long long MinTen : 3;   // minutes tens
};

struct wwvbBuffer *wwvbFrame;
unsigned long long receiveBuffer;
unsigned long long lastFrameBuffer;

int bcd2dec(int bcd)
{
  return ((bcd >> 4) * 10) + (bcd % 16);
}

int dec2bcd(int dec)
{
  return ((dec / 10) << 4) + (dec % 10);
}

char *buildTimeString()
{
  char rv[255];
  sprintf(rv, "%0.2i:%0.2i:%0.2i UTC   %c",
          bcd2dec(hour),
          bcd2dec(minute),
          bcd2dec(second),
          lastBit);
  return rv;
}

int sumFrameErrors()
{

  // Sum all of the values in our error buffer
  int i, rv;
  for (i = 0; i < 10; i++)
  {
    rv += errors[i];
  }

  return rv;
}

void updateDisplay()
{
  // Turn off the front panel light marking a successfully
  // received frame after 10 seconds of being on.
  if (bcd2dec(second) >= 10)
  {
    digitalWrite(lightPin, LOW);
  }

  // Update the first row
  char *time = buildTimeString();
  // lcd.print(time);

  // Update the second row
  // Cycle through our list of status messages
  // lcd.setCursor(0, 1);
  int cycle = bcd2dec(second) / 10; // This gives us 6 slots for messages
  char msg[17];                     // 16 chars per line on display

  switch (cycle)
  {
  // Show the Date
  case 0:
  {
    sprintf(msg, "%s %0.2i 20%0.2i",
            months[bcd2dec(month) - 1], bcd2dec(date), bcd2dec(year));
    break;
  }

  // Show the WWVB signal strength based on the # of recent frame errors
  case 1:
  {
    int signal = (10 - sumFrameErrors()) / 2;
    sprintf(msg, "WWVB Signal: %i", signal);
    break;
  }

  // Show LeapYear and LeapSecond Warning bits
  case 2:
  {
    const char *leapyear = (((byte)wwvbFrame->Leapyear) == 1) ? "Yes" : "No";
    const char *leapsec = (((byte)wwvbFrame->Leapsec) == 1) ? "Yes" : "No";
    sprintf(msg, "LY: %s  LS: %s", leapyear, leapsec);
    break;
  }

  // Show our Daylight Savings Time status
  case 3:
  {
    switch ((byte)wwvbFrame->Dst)
    {
    case 0:
      sprintf(msg, "DST: No");
      break;
    case 1:
      sprintf(msg, "DST: Ending");
      break;
    case 2:
      sprintf(msg, "DST: Starting");
      break;
    case 3:
      sprintf(msg, "DST: Yes");
      break;
    }
    break;
  }

  // Show the UT1 correction sign and amount
  case 4:
  {
    char sign;
    if ((byte)wwvbFrame->OffSign == 2)
    {
      sign = '-';
    }
    else if ((byte)wwvbFrame->OffSign == 5)
    {
      sign = '+';
    }
    else
    {
      sign = '?';
    }
    sprintf(msg, "UT1 %c 0.%i", sign, (byte)wwvbFrame->OffVal);
    break;
  }

  // Show the time and date of the last successfully received
  // wwvb frame
  case 5:
  {
    sprintf(msg, "[%s]", lastTimeUpdate);
    break;
  }
  }

  // lcd.print(msg);
}

void logFrameError(boolean err)
{

  // Add a 1 to log errors to the buffer
  int add = err ? 1 : 0;
  errors[errIdx] = add;

  // and move the buffer loop-around pointer
  if (++errIdx >= 10)
  {
    errIdx = 0;
  }
}
void buffer(int bit)
{

  // Process our bits
  if (bit == 0)
  {
    lastBit = '0';
    if (debug)
    {
      Serial.print("0");
    }
  }
  else if (bit == 1)
  {
    lastBit = '1';
    if (debug)
    {
      Serial.print("1");
    }
  }
  else if (bit == -1)
  {
    lastBit = 'M';
    if (debug)
    {
      Serial.print("M");
    }
    framePosition++;
  }
  else if (bit == -2)
  {
    lastBit = 'X';
    if (debug)
    {
      Serial.print("X");
    }
  }

  // Push the bit into the buffer.  The 0s and 1s are the only
  // ones we care about.
  if (bit < 0)
  {
    bit = 0;
  }
  receiveBuffer = receiveBuffer | ((unsigned long long)bit << (64 - bitPosition));

  // And increment the counters that keep track of where we are
  // in the frame.
  bitPosition++;
}

void debugPrintFrame()
{

  char time[255];
  byte minTen = (byte)wwvbFrame->MinTen;
  byte minOne = (byte)wwvbFrame->MinOne;
  byte hourTen = (byte)wwvbFrame->HourTen;
  byte hourOne = (byte)wwvbFrame->HourOne;
  byte dayHun = (byte)wwvbFrame->DayHun;
  byte dayTen = (byte)wwvbFrame->DayTen;
  byte dayOne = (byte)wwvbFrame->DayOne;
  byte yearOne = (byte)wwvbFrame->YearOne;
  byte yearTen = (byte)wwvbFrame->YearTen;

  byte wwvb_minute = (10 * minTen) + minOne;
  byte wwvb_hour = (10 * hourTen) + hourOne;
  byte wwvb_day = (100 * dayHun) + (10 * dayTen) + dayOne;
  byte wwvb_year = (10 * yearTen) + yearOne;

  sprintf(time, "\nFrame Decoded: %0.2i:%0.2i  %0.3i  20%0.2i\n",
          wwvb_hour, wwvb_minute, wwvb_day, wwvb_year);
  Serial.print(time);
}

void processBit()
{

  // Clear the bitReceived flag, as we're processing that bit.
  bitReceived = false;

  // determine the width of the received pulse
  unsigned int pulseWidth = pulseEndTime - pulseStartTime;

  // Attempt to decode the pulse into an Unweighted bit (0),
  // a Weighted bit (1), or a Frame marker.

  // Pulses < 0.2 sec are an error in reception.
  if (pulseWidth < 100)
  {
    buffer(-2);
    bitError++;
    wasMark = false;

    // 0.2 sec pulses are an Unweighted bit (0)
  }
  else if (pulseWidth < 400)
  {
    buffer(0);
    wasMark = false;

    // 0.5 sec pulses are a Weighted bit (1)
  }
  else if (pulseWidth < 700)
  {
    buffer(1);
    wasMark = false;

    // 0.8 sec pulses are a Frame Marker
  }
  else if (pulseWidth < 900)
  {

    // Two Frame Position markers in a row indicate an
    // end/beginning of frame marker
    if (wasMark)
    {

      // For the display update
      lastBit = '*';
      if (debug)
      {
        Serial.print("*");
      }

      // Verify that our position data jives with this being
      // a Frame start/end marker
      if ((framePosition == 6) &&
          (bitPosition == 60) &&
          (bitError == 0))
      {

        // Process a received frame
        frameEndTime = pulseStartTime;
        lastFrameBuffer = receiveBuffer;
        digitalWrite(lightPin, HIGH);
        logFrameError(false);

        if (debug)
        {
          debugPrintFrame();
        }
      }
      else
      {

        frameError = true;
      }

      // Reset the position counters
      framePosition = 0;
      bitPosition = 1;
      wasMark = false;
      bitError = 0;
      receiveBuffer = 0;

      // Otherwise, this was just a regular frame position marker
    }
    else
    {

      buffer(-1);
      wasMark = true;
    }

    // Pulses > 0.8 sec are an error in reception
  }
  else
  {
    buffer(-2);
    bitError++;
    wasMark = false;
  }

  // Reset everything if we have more than 60 bits in the frame.  This means
  // the frame markers went missing somewhere along the line
  if (frameError == true || bitPosition > 60)
  {

    // Debugging
    if (debug)
    {
      Serial.print("  Frame Error\n");
    }

    // Reset all of the frame pointers and flags
    frameError = false;
    logFrameError(true);
    framePosition = 0;
    bitPosition = 1;
    wasMark = false;
    bitError = 0;
    receiveBuffer = 0;
  }
}

void incrementWwvbMinute()
{

  // Increment the Time and Date
  if (++(wwvbFrame->MinOne) == 10)
  {
    wwvbFrame->MinOne = 0;
    wwvbFrame->MinTen++;
  }

  if (wwvbFrame->MinTen == 6)
  {
    wwvbFrame->MinTen = 0;
    wwvbFrame->HourOne++;
  }

  if (wwvbFrame->HourOne == 10)
  {
    wwvbFrame->HourOne = 0;
    wwvbFrame->HourTen++;
  }

  if ((wwvbFrame->HourTen == 2) && (wwvbFrame->HourOne == 4))
  {
    wwvbFrame->HourTen = 0;
    wwvbFrame->HourOne = 0;
    wwvbFrame->DayOne++;
  }

  if (wwvbFrame->DayOne == 10)
  {
    wwvbFrame->DayOne = 0;
    wwvbFrame->DayTen++;
  }

  if (wwvbFrame->DayTen == 10)
  {
    wwvbFrame->DayTen = 0;
    wwvbFrame->DayHun++;
  }

  if ((wwvbFrame->DayHun == 3) &&
      (wwvbFrame->DayTen == 6) &&
      (wwvbFrame->DayOne == (6 + (int)wwvbFrame->Leapyear)))
  {
    // Happy New Year.
    wwvbFrame->DayHun = 0;
    wwvbFrame->DayTen = 0;
    wwvbFrame->DayOne = 1;
    wwvbFrame->YearOne++;
  }

  if (wwvbFrame->YearOne == 10)
  {
    wwvbFrame->YearOne = 0;
    wwvbFrame->YearTen++;
  }

  if (wwvbFrame->YearTen == 10)
  {
    wwvbFrame->YearTen = 0;
  }
}

void wwvbChange()
{

  int signalLevel = digitalRead(wwvbInPin);

  // Determine if this was triggered by a rising or a falling edge
  // and record the pulse low period start and stop times
  if (signalLevel == LOW)
  {
    pulseStartTime = millis();
  }
  else
  {
    pulseEndTime = millis();
    bitReceived = true;
  }
}

void setup()
{
  if (debug)
  {
    Serial.begin(115200);
  }
  if (debug)
  {
    Serial.println("Ready.");
  }

  pinMode(wwvbInPin, INPUT);
  attachInterrupt(0, wwvbChange, CHANGE);

  lastFrameBuffer = 0;
  receiveBuffer = 0;
  wwvbFrame = (struct wwvbBuffer *)&lastFrameBuffer;
}

void loop()
{
  if (bitReceived == true)
  {
    processBit();
  }
}
