// https://forum.arduino.cc/index.php?topic=14946.msg109766#msg109766

#pragma once

#include <stdio.h>

class WWVBDecoder
{
private:
  static const char BIT_ZERO = 0;
  static const char BIT_ONE = 1;
  static const char BIT_MARKER = 2;
  static const char BIT_UNKNOWN = 3;

  unsigned short wwvbPin;

  int frame[60];
  unsigned long pulseDuration;
  unsigned short bitCount;
  int lastBit;

public:
  int year;
  int month;
  int day;
  int dayOfYear;
  bool isLeapYear;

  int hours;
  int minutes;
  int seconds;

  WWVBDecoder(unsigned int _wwvbInPin);

  void loop();
  void parseTime();
  void parseDate();
  bool isValidFrame();
  void printBit();
  void printFrame();
};
