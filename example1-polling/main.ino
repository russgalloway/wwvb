#include <wwvb-decoder.h>

#define WWVB_PIN 2

WWVBDecoder decoder(WWVB_PIN);

void setup()
{
  Serial.begin(115200);
  pinMode(WWVB_PIN, INPUT);
}

void loop()
{
  decoder.loop();
}
