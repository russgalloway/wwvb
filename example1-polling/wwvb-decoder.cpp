#include <Arduino.h>
#include "wwvb-decoder.h"

WWVBDecoder::WWVBDecoder(unsigned int _wwvbInPin)
{
  wwvbPin = _wwvbInPin;

  pulseDuration = 0;
  lastBit = BIT_MARKER;
  bitCount = 1;

  month = 0;
  day = 0;
  year = 0;
  isLeapYear = false;
  dayOfYear = 0;

  hours = 0;
  minutes = 0;
  seconds = 0;
}

void WWVBDecoder::loop()
{
  while (bitCount <= 60)
  {
    pulseDuration = pulseIn(wwvbPin, LOW, 1000000);

    if (pulseDuration >= 700000)
    {
      if (lastBit == BIT_MARKER)
      {
        bitCount = 61;
        continue;
      }

      lastBit = BIT_MARKER;
    }
    else if (pulseDuration >= 400000)
    {
      lastBit = BIT_ONE;
    }
    else if (pulseDuration >= 100000)
    {
      lastBit = BIT_ZERO;
    }
    else
    {
      lastBit = BIT_UNKNOWN;
    }

    frame[bitCount] = lastBit;
    bitCount++;

    printBit();
  }

  printFrame();

  bitCount = 1;
  frame[0] = BIT_MARKER;
}

void WWVBDecoder::printBit()
{
  switch (lastBit)
  {
  case BIT_MARKER:
    Serial.print("*");
    break;
  case BIT_ZERO:
    Serial.print("0");
    break;
  case BIT_ONE:
    Serial.print("1");
    break;
  case BIT_UNKNOWN:
    Serial.print("_");
    break;
  }
}

void WWVBDecoder::printFrame()
{
  Serial.print(" | ");

  if (isValidFrame())
  {
    parseTime();
    Serial.print(hours);
    Serial.print("h ");
    Serial.print(minutes);
    Serial.print("m ");
    Serial.print(seconds);
    Serial.print("s ");

    parseDate();
    Serial.print(month);
    Serial.print("/");
    Serial.print(day);
    Serial.print("/");
    Serial.print(year);
  }
  else
  {
    Serial.print("BAD FRAME");
  }

  Serial.println();
}

bool WWVBDecoder::isValidFrame()
{
  for (int i = 0; i < 60; i++)
    if (frame[i] == BIT_UNKNOWN)
      return false;

  return true;
}

void WWVBDecoder::parseTime()
{
  minutes = (frame[1] * 40) + (frame[2] * 20) + (frame[3] * 10) + (frame[5] * 8) + (frame[6] * 4) + (frame[7] * 2) + (frame[8] * 1);
  hours = (frame[12] * 20) + (frame[13] * 10) + (frame[15] * 8) + (frame[16] * 4) + (frame[17] * 2) + (frame[18] * 1);
  hours += (frame[57] * 1); // DST adjust
  hours -= 6;               // CST time zone adjust
  hours += hours < 0 ? 24 : 0;

  // actual time is 1 minute ago since it takes 1 minutes to receive the frame
  minutes += 1;

  if (minutes == 60)
  {
    minutes = 0;
    hours += 1;
  }
}

void WWVBDecoder::parseDate()
{
  dayOfYear = (frame[22] * 200) + (frame[23] * 100) + (frame[25] * 80) + (frame[26] * 40) + (frame[27] * 20) + (frame[28] * 10) + (frame[30] * 8) + (frame[31] * 4) + (frame[32] * 2) + (frame[33] * 1);
  year = (frame[45] * 80) + (frame[46] * 40) + (frame[47] * 20) + (frame[48] * 10) + (frame[50] * 8) + (frame[51] * 4) + (frame[52] * 2) + (frame[53] * 1);
  isLeapYear = frame[55];

  if (dayOfYear <= 31)
  {
    // January
    month = 1;
    day = dayOfYear;
  }
  else if (dayOfYear <= 59)
  {
    // February (no leap year)
    month = 2;
    day = dayOfYear - 31;
  }
  else if (dayOfYear <= 90)
  {
    // March
    month = 3;
    day = dayOfYear - 59;
  }
  else if (dayOfYear <= 120)
  {
    // April
    month = 4;
    day = dayOfYear - 90;
  }
  else if (dayOfYear <= 151)
  {
    // May
    month = 5;
    day = dayOfYear - 120;
  }
  else if (dayOfYear <= 181)
  {
    // June
    month = 6;
    day = dayOfYear - 151;
  }
  else if (dayOfYear <= 212)
  {
    // July
    month = 7;
    day = dayOfYear - 181;
  }
  else if (dayOfYear <= 243)
  {
    // August
    month = 8;
    day = dayOfYear - 212;
  }
  else if (dayOfYear <= 273)
  {
    // September
    month = 9;
    day = dayOfYear - 243;
  }
  else if (dayOfYear <= 304)
  {
    // October
    month = 10;
    day = dayOfYear - 273;
  }
  else if (dayOfYear <= 334)
  {
    // November
    month = 11;
    day = dayOfYear - 304;
  }
  else if (dayOfYear <= 365)
  {
    //December
    month = 12;
    day = dayOfYear - 334;
  }
  else
  {
    // ERROR
  }
}